# Exercise 8
## Guide Line

### Fetch latest version of kernel & buildroot sources
- `git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git --depth 1 --branch v5.13`
- `git clone git://git.buildroot.net/buildroot`

### Run initial script
- `source initial_script`

### Configure kernel & buildroot

#### Prepare build directory
- `make O=${BUILD_KERNEL} i386_defconfig`
- `make O=${BUILD_ROOTFS} qemu_x86_defconfig`

> For raspberry pi zero
> - `make ARCH=arm O=${BUILD_KERNEL_ARM} bcm2835_defconfig`
> - `make ARCH=arm CROSS_COMPILE=arm-none-eabi- O=${BUILD_ROOTFS_ARM} raspberrypi0w_defconfig`

#### Configure kernel
- `cd $(BUILD_KERNEL)`

> For raspberry pi zero
> - `cd $(BUILD_KERNEL_ARM)`

- `make menuconfig`

#### Build kernel
`make -j8` (_For 8 threads_)

#### Configure buildroot
- `cd $(BUILD_ROOTFS)` or `cd $(BUILD_ROOTFS_ARM)`
- and launch `make menuconfig`

---

### Target options --->
- Target Architecture **(i386)**
- Target Architecture Variant **(i686)**
### Toolchain --->
- Custom kernel headers series **(5.13.x)**
-  **[\*]** Enable WCHAR support
### System configuration --->
- **(Lnux)** System hostname
- **(Welcome Lnux)** System banner
- **[\*]** Enable root login with password
- **(root password)** Root password 
- **(${BUILD_ROOTFS}/users)** Path to the users tables
- **(${BUILD_ROOTFS}/root)** Root filesystem overlay directories
### Kernel --->
- **[ ]** Linux Kernel
### Target packages --->
- **[\*]** Show packages that are also provided by busybox
- **Development tools --->**
	- **[\*]** binutils
	- **[\*]** binutils binaries
	- **[\*]** findutils
	- **[\*]** grep
	- **[\*]** sed 
	- **[\*]** tree
- **Libraries ---> Compression and decompression --->**
	- **[\*]** zlib support
- **Libraries ---> Text and terminal handling --->**
	- **[\*]** ncurses
	- **[\*]** readline
- **Networking applications --->**
	- **[\*]** dropbear( ssh server )
	- **[\*]** openssh
	- **[\*]** wget
- **Shell and utilities --->**
	- **[\*]** bash
	- **[\*]** file
	- **[\*]** sudo
	- **[\*]** which
- **System tools --->**
	- **[\*]** kmod
	- **[\*]** kmod utilities
	- **[\*]** rsyslog
- **Text editors and viewers --->**
	- **[\*]** less
	- **[\*]** mc
    - **[\*]** nano

### Filesystem images --->
- **[\*]** ext2/3/4 root filesystem
- ext2/3/4 variant **(ext4)**
- **[\*]** tar the root filesystem

## For raspberry pi zero

### Toolchain --->
- Custom kernel headers series **(5.13.x)**
-  **[\*]** Enable WCHAR support
### System configuration --->
- **(Lnux)** System hostname
- **(Welcome Lnux)** System banner
- **[\*]** Enable root login with password
- **(root password)** Root password 
- **(${BUILD_ROOTFS}/users)** Path to the users tables
- **(${BUILD_ROOTFS}/root)** Root filesystem overlay directories
### Kernel --->
- **[ ]** Linux Kernel
### Target packages --->
- **[\*]** Show packages that are also provided by busybox
- **Development tools --->**
	- **[\*]** binutils
	- **[\*]** binutils binaries
	- **[\*]** findutils
	- **[\*]** grep
	- **[\*]** sed 
	- **[\*]** tree
- **Libraries ---> Compression and decompression --->**
	- **[\*]** zlib support
- **Libraries ---> Text and terminal handling --->**
	- **[\*]** ncurses
	- **[\*]** readline
- **Networking applications --->**
	- **[\*]** dropbear( ssh server )
	- **[\*]** wget
	- **[\*]** wpa_supplicant
	- **[\*]** wpa_supplicant - Enable 80211 support
- **Hardware handling ---> Firmware --->**
	- **[\*]** rpi-wifi-firmware
- **Shell and utilities --->**
	- **[\*]** bash
	- **[\*]** file
	- **[\*]** sudo
	- **[\*]** which
- **System tools --->**
	- **[\*]** kmod
	- **[\*]** kmod utilities
	- **[\*]** rsyslog
- **Text editors and viewers --->**
	- **[\*]** less
	- **[\*]** mc
    - **[\*]** nano

### Filesystem images --->
- **[\*]** ext2/3/4 root filesystem
- ext2/3/4 variant **(ext4)**
- **[\*]** tar the root filesystem

---
- additional files
```bash
# Create user record
echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users
# Add user user to sodoers
mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d  
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user
# Create list of shells for dropbear
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells  
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
```
- and for raspberry pi zero w
```bash
# Create user record
echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS_ARM}/users
# Add user user to sodoers
mkdir -p ${BUILD_ROOTFS_ARM}/root/etc/sudoers.d
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS_ARM}/root/etc/sudoers.d/user
# Create list of shells for dropbear
echo "/bin/sh" > ${BUILD_ROOTFS_ARM}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS_ARM}/root/etc/shells
```

- build ROOTFS
	- `make -j8` (_For use 8 threads_)
- install QEMU (in ArchLinux)
	- `sudo pacman -S qemu qemu-extra`
- launch QEMU
    - run `./run_qemu`
- screen system info
    - run `./info`

## For raspberry pi
- Create a file, named `interfaces` in `${BUILD_ROOTFS}/etc/wpa_supplicant.conf`
```
auto lo
iface lo inet loopback
 
auto eth0
iface eth0 inet dhcp
    pre-up /etc/network/nfs_check
    wait-delay 15
 
auto wlan0
iface wlan0 inet dhcp
    pre-up wpa_supplicant -D nl80211 -i wlan0 -c /etc/wpa_supplicant.conf -B
    post-down killall -q wpa_supplicant
    wait-delay 15
 
iface default inet dhcp
```
- Create another file, named `wpa_supplicant.conf` with `wpa_passphrase` in `${BUILD_ROOTFS_ARM}/etc/network/`
```
ctrl_interface=/var/run/wpa_supplicant
ap_scan=1
 
network={
   ssid="EDIT_THIS"
   psk="EDIT_THIS"
}
```
- and add the next line in file `/etc/ssh/sshd_config`
```
PermitRootLogin yes
PermitEmptyPassword yes
```
